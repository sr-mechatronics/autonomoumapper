/**
 * Stefan Reichenstein
 * Cora Dimmig
 * Baxter Eldridge
 * Lab 5 - Autonomous Mapper
 * 
 * Use the Pixy to determine a line.
 * Use the Pixy to determine distance to a point (width based).
 * Use this distance to take measurements on both sides of the Pixy (PING))) US Sensors).
 * Use DC motors to move (must make small adjustments and see where we are).
 */


// MUST USE A MEGA
// Not enough pins for Bidirectional PWM motor control + SPI
#include <SPI.h>
#include <SD.h>
#include <Pixy.h>

/*
 * SPI Pins
 * Uno -  11-13, 10 = Default SS
 * Mega - 50-52, 53 = Default SS
 */
/*
 * Mega Pixy Pins
 * Gnd - Black
 * 50 (MISO) - Blue
 * 51 (MOSI) - Purple
 * 52 (SCK)  - Gray
 * 53 (SS)   - White
 */
const int PIN_SD_SS = 20;

/*
 * PWM Pins
 * Uno -  3, 5, 6, 9, 10, 11 (5, 6 are run higher than expected. 0% duty cycle impossible)
 * Mega - 2-13, 44-46
 */
const int PIN_MOTOR_L_R = 2;
const int PIN_MOTOR_L_F = 3;

const int PIN_MOTOR_R_F = 4;
const int PIN_MOTOR_R_R = 5;

const int PIN_RANGEFINDER_L = 32;
const int PIN_RANGEFINDER_R = 22;

const int PIN_R1_LED = 16;
const int PIN_R2_LED = 17;
const int PIN_G1_LED = 18;

double left_pwm = 45, right_pwm = 54;


/*
 * PIXY
 */
Pixy pixy;
const int CENTER_X = 160;
const int ERR = 3;

/*
 * DATA LOGGER
 */
const char FILE_NAME[] = "map.csv";
File f;

/*
 * Environment Data
 */
const int NUM_PINGS = 10;

const long DELTA_TIME_MOVEMENT = 20;  // millis
const long DELTA_TIME_FRAME = 20 / 4;   // millis

/*
 * TIME COUNTERS
 */
long delta;
long start;

int pingCount = 0;

int desiredWidth[] = {15, 12, 175};
int color = 10;
// blue is 1, green is 2


void setup() {
  //#if DEV
  Serial.begin(9600);
  while (!Serial) {
    Serial.println("Waiting for Serial to begin...");
  }

  // Initialize Pins

  pinMode(PIN_R1_LED, OUTPUT);
  pinMode(PIN_R2_LED, OUTPUT);
  pinMode(PIN_G1_LED, OUTPUT);

  pinMode(PIN_MOTOR_L_F, OUTPUT);
  pinMode(PIN_MOTOR_L_R, OUTPUT);
  pinMode(PIN_MOTOR_R_F, OUTPUT);
  pinMode(PIN_MOTOR_R_R, OUTPUT);

  // init pixy
  pixy.init();

  // Code for SD shield (not used)
  /*
    while (!SD.begin(PIN_SD_SS)) {
    // turn on red led?
    //#if DEV
    sprintf(str, "Error initializing SD with SS Pin %d", PIN_SD_SS);
    Serial.println(str);
    //#endif
    while (true) { digitalWrite(PIN_R1_LED, HIGH); }
    }
    f = SD.open(FILE_NAME, FILE_WRITE);
    if (!f) {
    //#if DEV
    sprintf(str, "Error opening data file: %s", FILE_NAME);
    Serial.println(str);
    //#endif
    while (true) { digitalWrite(PIN_R2_LED, HIGH); }
    }

    // turn on green led
    //#if DEV
    sprintf(str, "SD initialized with SS Pin %d", PIN_SD_SS);
    Serial.println(str);
    //#endif
  */
}

void loop() {
  // IF not at end of track
  //   center the camera by correcting "left" or "right" (record distance?)
  //   IF distance to next reading is under a threshold, then take reading (save to disk)
  //   determine distance to the next reading.
  //   move by the minimum step. while recording distance


  // move forward some distance
  // align -> compute delta distance if any
  
  
  /*
   * General structure of the code (refined from above comments).
   * 
   * If the Pixy sees a long stip of blue tape (ie. at the end), stop and loop forever.
   * Else Pixy is not at the end and:
   * 	1) Get the distance and take measurements accordingly
   * 	2) Move forward a little while correcting alignment
   * REPEAT
   */
  if (pixy.getBlocks() > 0 && pixy.blocks[0].width > 220) {
    Serial.println("END");
    analogWrite(PIN_MOTOR_L_F, 0);
    analogWrite(PIN_MOTOR_R_F, 0);
    while (1) {
    }
  } else {
    getDistance();		// Get distance and take readings
    move();				// move and correct
  }
}

/**
 * Get this distance by checking recognized widths.
 * Store and only look for alternate colors so as to not double count.
 */
void getDistance() {
  int blocks;
  int identified = 0;
  identified = 0;
  int currentcolor;
  start = millis();
  
  // Check a few frames (aprox 10. should be less than 10)
  while (abs(millis() - start) < 200) {
    blocks = pixy.getBlocks();
	
	// iterate over blocks
    for (int j = 0; j < blocks; j++) {
      //pixy.blocks[j].print();
	  
	  // if the width of the current blocks minus the calibrated width is less
	  // than an error theshold AND if the color is not the same as the previously
	  // recognized one, set the current color and continue
      if (abs(pixy.blocks[j].width - desiredWidth[pixy.blocks[j].signature - 1]) < 5) {
        if (pixy.blocks[j].signature != color) {
          identified++;
          currentcolor = pixy.blocks[j].signature;
        }
      }
    }
    delay(15);	// wait for a little less than the duration of a Pixy frame refresh
  }

  // if the color was identified in over 2 frames (out of about 10), stop and take a reading
  if (identified > 1) {
    color = currentcolor;
    //Serial.println("Stop");
    //Serial.println(currentcolor);
    analogWrite(PIN_MOTOR_L_F, 0);
    analogWrite(PIN_MOTOR_R_F, 0);
    getWriteReading();
    analogWrite(PIN_MOTOR_L_F, left_pwm );
    analogWrite(PIN_MOTOR_R_F, right_pwm );
  }
  else {
    //Serial.println("Go");
  }
  delay(150);
}

/**
 * Move by a little amount, then correct.
 */
void move() {
  delta = 0;
  start = millis();
  
  // for a small amount of time (less than 3 Pixy frames)
  while (delta < DELTA_TIME_MOVEMENT) {
	// set the forward speeds
    analogWrite(PIN_MOTOR_L_F, left_pwm);
    analogWrite(PIN_MOTOR_R_F, right_pwm);

	
	// make small corrections. maybe this should be flipped back?
	// SR - change order if we get a chance to test again
    center();
	
	// delay a little (ie move forward)
	delta = millis() - start;
    if (delta < DELTA_TIME_FRAME) {
      delay(DELTA_TIME_FRAME - delta);
    }
  }
}

/**
 * Centers and delays to allow motors to move small amounts
 */
void center() {
  int i = pixy.getBlocks();
  if (i < 1) {
    return;
  }

  // gets the closest block
  int x = getLargestBlockX(i);

  // gets the difference of the closest block and then adjusts left or right by small amounts.
  // x and delta are updated in the loop
  int delta = x - CENTER_X;
  while (-ERR < delta < ERR) {
    delta = x - CENTER_X;
    if (delta > ERR) {
      // camera looking left. rotate right
      analogWrite(PIN_MOTOR_L_F, left_pwm);
      analogWrite(PIN_MOTOR_R_F, 0);
    } else if (delta < -ERR) {
      // camera looking right. rotate left
      analogWrite(PIN_MOTOR_L_F, 0);
      analogWrite(PIN_MOTOR_R_F, right_pwm);
    } else {

    }
    delay(5);
    i = pixy.getBlocks();
    if (i < 1) {
      return;
    }
    x = getLargestBlockX(i);
    delta = x - CENTER_X;
  }
}

/**
 * Iterate over available blocks and find the closest one.
 * was orriginally the largest one, hence the name.
 */
int getLargestBlockX(int i) {
  // change this logic to work with width if closer (smaller) y values do not work
  int y = pixy.blocks[0].y, x = pixy.blocks[0].x;
  for (int j = 1; j < i; ++j) {
    if (pixy.blocks[j].y < y) {
      x = pixy.blocks[j].x;
      y = pixy.blocks[j].y;
    }
  }
  return x;
}
/**
 * Gets and writes the readings.
 */
boolean getWriteReading() {
  // takes the left then right readings
  double left = pingAverage(PIN_RANGEFINDER_L);
  double right = pingAverage(PIN_RANGEFINDER_R);

 // Serial.print("Left/Right Dist:\t");
 // Serial.print(left);
 // Serial.print("/");
 // Serial.println(right);

  return write(++pingCount, left, right);
}

/**
 * Takes the average of PING))) readings on a specified pin.
 */
double pingAverage(int pin) {
  double sum = 0;
  double cur = 0;
  start = micros() + 20000;
  for (int i = 0; i < NUM_PINGS; ++i) {
    // this is the time needed before
    while (abs(micros() - start) < 20000) {}
    cur = ping(pin);
    start = micros();
    sum += cur;
  }

  return sum / NUM_PINGS;
}

/**
 * PING))) Reading.
 */
double ping(int pin) {
  static const long ping_timeout_us = 18500;
  // reset PING))) TTL Signal
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delayMicroseconds(2);
  digitalWrite(pin, HIGH);
  delayMicroseconds(5);

  // wait for RTT
  digitalWrite(pin, LOW);

  // wait for pin to go high
  pinMode(pin, INPUT);
  while (digitalRead(pin) == LOW) {}
  long start_ping = micros();
  long delta;
  while (digitalRead(pin) == HIGH) {
    delta = abs(micros() - start_ping);
    if (delta >= 18500) {
      // timedout
      Serial.println("Timeout");
      return 300.0;
    }
  }

  /*
  // whats the timeout time?!?!?!?
  double ret = pulseIn(pin, HIGH, 19);  // pulseIn(int pin, int Value, long timeout=1second)
  */
  // Serial.println(delta * 0.01715);
  return delta * 0.01715; // should be HALF the RTT in air at sea level at ? degrees.
}

/**
 * Write format is csv. Order of write is: LEFT,DIST,RIGHT
 */
boolean write(double distance, double left_dist, double right_dist) {
  // write as csv
  Serial.print(left_dist);
  Serial.print(",");
  Serial.print(distance);
  Serial.print(",");
  Serial.println(right_dist);
  return true;
}
